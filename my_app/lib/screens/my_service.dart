import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_app/widget/add_list_product.dart';
import 'home.dart';
import 'package:my_app/widget/show_list_product.dart';
class MyService extends StatefulWidget {
  @override
  _MyServiceState createState() => _MyServiceState();
}

class _MyServiceState extends State<MyService> {
  //Variable
  String login = '...';
  Widget currentWidget = ShowListProduct();
  //Method
  @override
  void initState() {
    super.initState();
    findDisplayName();
  }

  Future<void> findDisplayName() async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    FirebaseUser firebaseUser = await firebaseAuth.currentUser();
    setState(() {
      login = firebaseUser.displayName;
    });
    print('Login => $login');
  }

  //Widget
  Widget showHead() {
    return DrawerHeader(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/bg_drawer.jpg'), fit: BoxFit.cover)),
      child: Column(
        children: <Widget>[
          showLogo(),
          showAppName(),
          SizedBox(
            height: 10.0,
          ),
          showLogin(),
        ],
      ),
    );
  }

  Widget showAppName() {
    return Text('My App',
        style: TextStyle(
            color: Colors.blue.shade700,
            fontWeight: FontWeight.bold,
            fontSize: 18.0));
  }

  Widget showLogin() {
    return Text(
      'Login By $login',
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 15.0,
      ),
    );
  }

  Widget showLogo() {
    return Container(
      width: 80.0,
      height: 80.0,
      child: Image.asset('images/user_login.png'),
    );
  }

  Widget showListProduct() {
    return ListTile(
      leading: Icon(
        Icons.list,
        size: 36.0,
      ),
      title: Text('List Product'),
      subtitle: Text('show all list'),
      onTap: () {
        setState(() {
          currentWidget = ShowListProduct();
        });
        Navigator.of(context).pop();
      },
    );
  }

  Widget showListAddList() {
    return ListTile(
      leading: Icon(
        Icons.playlist_add,
        size: 36.0,
      ),
      title: Text('Add List Product'),
      //subtitle: Text('click for add'),
      onTap: () {
        setState(() {
          currentWidget = AddListProduct();
        });
        Navigator.of(context).pop();
      },
    );
  }

  Widget showDrawer() {
    return Drawer(
      child: ListView(
        children: <Widget>[
          showHead(),
          showListProduct(),
          showListAddList(),
        ],
      ),
    );
  }

  Widget singOutButton() {
    return IconButton(
      icon: Icon(Icons.exit_to_app),
      tooltip: 'Sing Out',
      onPressed: () {
        myAlert();
      },
    );
  }

  //Logout
  void myAlert() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are your Sure?'),
            content: Text('Do your want Sing out!'),
            actions: <Widget>[cancellButon(), oklButon()],
          );
        });
  }

  Widget oklButon() {
    return FlatButton(
      child: Text('OK'),
      onPressed: () {
        Navigator.of(context).pop();
        //add function logout
        processSingOut();
      },
    );
  }

  Widget cancellButon() {
    return FlatButton(
      child: Text('Cancel'),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

//Function Logout
  Future<void> processSingOut() async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    await firebaseAuth.signOut().then((response) {
      MaterialPageRoute materialPageRoute =
          MaterialPageRoute(builder: (BuildContext context) => Home());
      Navigator.of(context).pushAndRemoveUntil(
          materialPageRoute, (Route<dynamic> route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Service'),
        actions: <Widget>[singOutButton()],
      ),
      drawer: showDrawer(),
      body: currentWidget
    );
  }
}

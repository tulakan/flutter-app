import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_app/screens/authen.dart';
import 'package:my_app/screens/my_service.dart';
import 'package:my_app/screens/register.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  //CHeck status Login first check
  @override
  void initState(){
    super.initState();
    checkStatus();
  }
  Future<void> checkStatus()async{
      FirebaseAuth firebaseAuth = FirebaseAuth.instance;
      FirebaseUser firebaseUser = await firebaseAuth.currentUser();
      if(firebaseUser != null){
        MaterialPageRoute materialPageRoute = MaterialPageRoute(builder: (BuildContext context)=>MyService());
        Navigator.of(context).pushAndRemoveUntil(materialPageRoute,(Route<dynamic>route) =>false);
      }
  }
  //End check login

Widget showText(){
  return Text('Flutter Project ',style: TextStyle(
    fontSize: 36.0,
    color: Colors.lightBlueAccent,
    fontWeight: FontWeight.bold
  ),);
}

Widget showImage(){
  return Container(
      width: 120.0,
      height: 120.0,
      child: Image.asset('images/user_login.png')
    );
}
Widget singInButton(){
  return RaisedButton(
    color: Colors.blue.shade700,
    child: Text('Sign In',style: TextStyle(color: Colors.white),),
    onPressed: (){
      MaterialPageRoute materialPageRoute = MaterialPageRoute(builder: (BuildContext context) => Authen());
      Navigator.of(context).push(materialPageRoute);
    },
    );
}
Widget singUpButton(){
  return OutlineButton( 
    child: Text('Sing Up'),
    onPressed: (){
      MaterialPageRoute materialPageRoute = new MaterialPageRoute(builder: (BuildContext context) => Register());
      Navigator.of(context).push(materialPageRoute);
    },
  );
}

Widget showButton(){
  return Row(
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      singInButton(),
      SizedBox(width: 5.0,),
      singUpButton()
    ],
  );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient:RadialGradient(  
              colors: [Colors.white,Colors.blue.shade400],
              radius: 1.5
            ) 
            ),
          child: Center(
            child:Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              showImage(),
              showText(),
              showButton()
          ],
          ) 
          ,),
        )
      ),
    );
  }
}
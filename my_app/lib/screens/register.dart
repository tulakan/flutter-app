

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:my_app/screens/authen.dart';
// import 'package:my_app/screens/my_service.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  //Variable
  final formKey = GlobalKey<FormState>();
  String nameString, emailString, passwordString;
  //Medhod
  Widget registerButton() {
    return IconButton(
      icon: Icon(Icons.cloud_upload),
      onPressed: () {
        print('Onclick Icon');
        if (formKey.currentState.validate()) {
          formKey.currentState.save();
          print(
              'name = $nameString ,email = $emailString, password = $passwordString');
          registerThread();
        }
      },
    );
  }
  void myAlert(String title, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: ListTile(
                leading: Icon(
                  Icons.add_alert,
                  color: Colors.red,
                  size: 48.0,
                ),
                title: Text(
                  title,
                  style: TextStyle(color: Colors.red),
                )),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
  Future<void> registerThread() async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    await firebaseAuth
        .createUserWithEmailAndPassword(
            email: emailString, password: passwordString)
        .then((response) {
      print('Register success $emailString');
      setUpDisplayName();
    }).catchError((response) {
      String title = response.code;
      String message = response.message;
      print('Title => $title, message => $message');
      myAlert(title, message);
    });
  }

//Route ไปหน้าต่อไป แต่ ย้อนกลับไม่ได้
Future<void> setUpDisplayName() async{
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  await firebaseAuth.currentUser().then((response){
    UserUpdateInfo userUpdateInfo = UserUpdateInfo();
    userUpdateInfo.displayName = nameString;
    response.updateProfile(userUpdateInfo);

    //Route not back
    MaterialPageRoute materialPageRoute = MaterialPageRoute(builder: (BuildContext context) => Authen());
    Navigator.of(context).pushAndRemoveUntil(materialPageRoute, (Route<dynamic> route) => false);
  }
  );

  
}

  Widget nameText() {
    return TextFormField(
      style: TextStyle(color: Colors.blue),
      decoration: InputDecoration(
          icon: Icon(
            Icons.face,
            color: Colors.blue,
            size: 40.0,
          ),
          labelText: 'Name',
          labelStyle:
              TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
          helperText: 'Please input name',
          helperStyle:
              TextStyle(color: Colors.blue, fontStyle: FontStyle.italic)),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Name is Not Null';
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        nameString = value;
      },
    );
  }

  Widget emailText() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(color: Colors.blue),
      decoration: InputDecoration(
          icon: Icon(
            Icons.email,
            color: Colors.blue,
            size: 40.0,
          ),
          labelText: 'Email',
          labelStyle:
              TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
          helperText: 'Please input Email',
          helperStyle:
              TextStyle(color: Colors.blue, fontStyle: FontStyle.italic)),
      validator: (String value) {
        if (!(value.contains('@')) && !(value.contains('.'))) {
          return 'Please check format Email you@mail.com';
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        emailString = value;
      },
    );
  }

  Widget passwordText() {
    return TextFormField(
      style: TextStyle(color: Colors.blue),
      decoration: InputDecoration(
          icon: Icon(
            Icons.lock,
            color: Colors.blue,
            size: 40.0,
          ),
          labelText: 'Password',
          labelStyle:
              TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
          helperText: 'Please input Password',
          helperStyle:
              TextStyle(color: Colors.blue, fontStyle: FontStyle.italic)),
      validator: (String value) {
        if (value.length < 7) {
          return 'Password shold mare 8 character';
        } else {
          return null;
        }
      },
      onSaved: (String value) {
        passwordString = value;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
        actions: <Widget>[
          registerButton(),
        ],
      ),
      body: Form(
        key: formKey,
        child: ListView(
          padding: EdgeInsets.all(30),
          children: <Widget>[nameText(), emailText(), passwordText()],
        ),
      ),
    );
  }
}
